<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
class NewContactControllerCore extends FrontController
{
    public $php_self = 'newcontact';
    public $ssl = true;

    /**
     * Assign template vars related to page content.
     *
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        if(Tools::isSubmit('sendmail')){
            $bussines = Tools::getValue('bussines');
            $name = Tools::getValue('name');
            $email = Tools::getValue('email');
            $phone = Tools::getValue('phone');
            $address = Tools::getValue('address');
            $message = Tools::getValue('message');
            Mail::send(
                (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
                'contact', // email template file to be use
                'Booking', // email subject
                array(
                    '{email}' => "$email", // sender email address
                    '{message}' => "bussines name: $bussines, name: $name, email: $email, phone: $phone, address: $address, message: $message", // email content
                ),
                Configuration::get('PS_SHOP_EMAIL'), // receiver email address
                NULL, //receiver name
                NULL, //from email address
                NULL  //from name
            );
            //Tools::redirect('index.php');
            $this->success ="Your message has been successfully sent to our team.";
            //return $message_p;
        }
        parent::initContent();

        $this->setTemplate('newcontact');
    }

    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Contact us', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('newcontact', true),
        ];

        return $breadcrumb;
    }
}
