{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file='page.tpl'}
{block name='page_header_container'}{/block}

{if $layout === 'layouts/layout-left-column.tpl'}
  {block name="left_column"}
    <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
      {widget name="ps_contactinfo" hook='displayLeftColumn'}
    </div>
  {/block}
{else if $layout === 'layouts/layout-right-column.tpl'}
  {block name="right_column"}
    <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
      {widget name="ps_contactinfo" hook='displayRightColumn'}
    </div>
  {/block}
{/if}

{block name='page_content'}
<section class="contact-form-2">

  <form method="POST">
  <section class="form-fields">
  
    <div class="form-group row">
      <h2>Request a booking or quote</h2>
      <p>Instructions: Fill out the details below, download our Work Order form, fill it out completely and upload it in the attachment section. We will get in touch with you soon .</p>
      {l s='' mod='contactform'}
    </div>  
      <div class="form-group row">
        <label class="col-md-3 form-control-label">Business name:</label>
        <div class="col-md-6">
          <input type="text" name="bussines" class="form-control" placeholder="Business name">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 form-control-label">Contact name:</label>
        <div class="col-md-6">
          <input type="text" name="name" class="form-control" placeholder="Contact name" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 form-control-label">Email:</label>
        <div class="col-md-6">
          <input type="text" name="email" class="form-control" placeholder="Email" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 form-control-label">Phone number:</label>
        <div class="col-md-6">
          <input type="text" name="phone" class="form-control" placeholder="Phone number" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 form-control-label">Address:</label>
        <div class="col-md-6">
          <input type="text" name="address" class="form-control" placeholder="Address" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 form-control-label">How can we help:</label>
        <div class="col-md-6">
          <textarea id="message" class="form-control" name="message" placeholder="How can we help?" rows="5" required></textarea>
        </div>
      </div>
      <div class="form-group row text-sm-center">
        <button type="submit" name="sendmail" class="btn btn-primary">
          {l s='Send message' mod='contactform'}
        </button>        
      </div>
      
    </section>
  </form>
</section>
{/block}
