{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{extends file=$layout}

{block name='header'}
  {include file='checkout/_partials/header.tpl'}
{/block}

{block name='content'}
  <section id="content" class="content-checkout">
  {$have_disclamer = 0}
  {foreach from=$cart.products item=product}
    {if $product.disclamer == 1}
      {$have_disclamer = 1}
    {/if}
  {/foreach}
  {if $have_disclamer == 1}
    <div class="row">
      <div class="cart-grid-right col-xs-12 col-lg-12 disclamer_text">
        <h6>Due to an increased amount of fraudulent orders we have increased our security for credit card transactions. Orders placed with a shipping address that differs from the billing address (unless the order is shipping to an FFL) may be delayed while we verify. Additionally, we may reach out to you to confirm the order. Please ensure that your billing address matches the information at your financial institution exactly. Capitalization, abbreviations, and punctuation MUST match. We are sorry for the inconvenience, but we must go through these steps to ensure we are able to conduct business.</h6>
      </div>
    </div>
    {/if}
    <div class="cart-grid-right col-xs-12 col-lg-12">
    <div class="tittle-checkout">ORDER SUMMARY</div>
        {block name='cart_summary'}
          {include file='checkout/_partials/cart-summary.tpl' cart=$cart}
        {/block}
        {hook h='displayReassurance'}
      </div>
      <div class="cart-grid-body col-xs-12 col-lg-12">
        {block name='checkout_process'}
          {render file='checkout/checkout-process.tpl' ui=$checkout_process}
        {/block}
      </div>
    </div>
  </section>
{/block}

{block name='footer'}
  {include file='checkout/_partials/footer.tpl'}
{/block}
